package th.ac.tu.siit.lab9googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		

		
		/*MarkerOptions mo = new MarkerOptions();
		mo.position(new LatLng(0,0));
		mo.title("Hello");
		
		map.addMarker(mo);*/
		/*
		PolylineOptions po = new PolylineOptions();
		po.add(new LatLng(0,0));
		po.add(new LatLng(13.75, 100.4667));
		po.width(5);
		
		map.addPolyline(po);
		*/
		
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				PolylineOptions po = new PolylineOptions();
				po.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				po.add(new LatLng(l.getLatitude(),l.getLongitude()));
				po.width(5);
				po.color(Color.RED);
				map.addPolyline(po);
				currentLocation = l;
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}

	
	
	
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int id = item.getItemId();
		switch (id) {
		case R.id.action_mark: 
			
			MarkerOptions mo = new MarkerOptions();
			mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
			mo.title("Hello");
			
			map.addMarker(mo);
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}






	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		
	
		return true;
	}

}
